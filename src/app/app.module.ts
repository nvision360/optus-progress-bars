import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { UserguideComponent } from './userguide/userguide.component';
import { ProgressDataService } from './services/progress.service';
import { RouterModule, Routes } from '@angular/router';
const appRoutes: Routes = [
  { path: '', component: UserguideComponent },
  { path: 'userguide', component: UserguideComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    UserguideComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpModule,
    CommonModule
  ],
  providers: [ProgressDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
