import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class ProgressDataService {
   
    public domain = `http://pb-api.herokuapp.com/bars`;

    constructor(private http: Http) {

    }

    getAll() {
    
      return this.http.get(this.domain).map(this.extractData);
    }
    extractData(res: Response) {
        let body = res.json();
            return body;
        }

}