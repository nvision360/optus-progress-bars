import { Component, OnInit } from '@angular/core';
import { $ } from 'protractor';
import { ProgressDataService } from '../services/progress.service';

@Component({
  selector: 'app-userguide',
  templateUrl: './userguide.component.html',
  styleUrls: ['./userguide.component.css']
})
export class UserguideComponent implements OnInit {

  constructor(private ProgressDataService: ProgressDataService) { }
 
  dataList: any = [];
  ngOnInit() {

    this.ProgressDataService.getAll().subscribe(data => {
    this.dataList.push(data);
  }, error => {
      console.log(error+ 'Api is not working');
     
  });

  }
  progressId = null;
  progressUpdate(i, value) {
    if ((this.dataList[i].bars[this.progressId] + value) < 0) {
      this.dataList[i].bars[this.progressId] = 0;
    } else {
      this.dataList[i].bars[this.progressId] = (this.dataList[i].bars[this.progressId] + value);
    }
  }
  ValueChange(i) {
    this.progressId = i;
  }
}
